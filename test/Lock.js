const {
  time,
  loadFixture,
} = require("@nomicfoundation/hardhat-network-helpers");
const { anyValue } = require("@nomicfoundation/hardhat-chai-matchers/withArgs");
const { expect } = require("chai");
const { ethers } = require("hardhat");
const { abi } = require("./abi.json");

describe("Price", function () {
  // We define a fixture to reuse the same setup in every test.
  // We use loadFixture to run this setup once, snapshot that state,
  // and reset Hardhat Network to that snapshot in every test.
  async function deployOneYearLockFixture() {
    const [owner, otherAccount] = await ethers.getSigners();
    const tokenPrice = await ethers.getContractFactory("tokenPrice");
    const contract = await tokenPrice.deploy();

    return { contract, owner, otherAccount };
  }

  describe("Deployment", function () {
    // it("Should set the right token price", async function () {
    //   const { contract } = await loadFixture(deployOneYearLockFixture);
    //   const tokenAddress = "0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56";
    //   const price = await contract.getPrice(tokenAddress);
    //   console.log(ethers.utils.formatEther(price.toString()));

    // });
    it("Should set the right token price", async function () {
      const provider = new ethers.providers.JsonRpcProvider(
        "http://localhost:8545"
      );

      // Obtener el número de bloque actual
      const blockNumber = await provider.getBlockNumber();
      console.log("Número de bloque actual:", blockNumber);

      // Obtener información del bloque
      const block = await provider.getBlock(blockNumber);
      console.log("Información del bloque actual:", block);
    });
  });
});
