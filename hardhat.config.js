require("@nomicfoundation/hardhat-toolbox");

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: "0.8.17",
  networks: {
    hardhat: {
      forking: {
        //eth url
        url: "https://eth.llamarpc.com/",
        timeout: 12000000,
      },
    },
  },
};
