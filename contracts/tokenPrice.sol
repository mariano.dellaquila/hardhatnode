// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

// Uncomment this line to use console.log
// import "hardhat/console.sol";
//pancakeswap router
interface IUniswapV2Router02 {
    function getAmountsOut(uint amountIn, address[] calldata path) external view returns (uint[] memory amounts);
    function WETH() external pure returns (address);
}



contract tokenPrice {
    address public owner;
    constructor() {
        owner = msg.sender;
    }

    IUniswapV2Router02 public uniswapV2Router02 = IUniswapV2Router02(0x10ED43C718714eb63d5aA57B78B54704E256024E);

    function getPrice(address token) public view returns (uint256) {
        address[] memory path = new address[](2);
        path[0] = uniswapV2Router02.WETH();
        path[1] = token;
        uint[] memory amounts = uniswapV2Router02.getAmountsOut(1e18, path);
        return amounts[1];
    }

}
